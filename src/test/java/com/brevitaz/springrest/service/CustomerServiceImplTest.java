package com.brevitaz.springrest.service;

import com.brevitaz.springrest.dao.CustomerDao;
import com.brevitaz.springrest.exception_handler.InvalidDataFoundException;
import com.brevitaz.springrest.exception_handler.ResourceNotFoundException;
import com.brevitaz.springrest.model.Customer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.io.IOException;
import java.sql.SQLException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@DisplayName("CustomerServiceImpl class")
@TestMethodOrder(MethodOrderer.DisplayName.class)
class CustomerServiceImplTest {

    @InjectMocks
    private CustomerServiceImpl customerServiceImpl;

    @Mock
    private CustomerDao customerDaoMock;

    public CustomerServiceImplTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("testing saveCustomer() with valid data")
    public void testSaveCustomerWithValidData() throws SQLException, IOException {
        Customer customer=new Customer();
        customer.setCustomerId(20);
        customer.setFirstName("aaa");
        customer.setLastName("bbb");
        customer.setPhoneNumber("9063659874");

        Customer customer1=new Customer();
        customer1.setCustomerId(20);
        customer1.setFirstName("aaa");
        customer1.setLastName("bbb");
        customer1.setPhoneNumber("9063659874");

        when(customerDaoMock.getCustomerId(customer)).thenReturn(-1);
        when(customerDaoMock.saveCustomer(customer)).thenReturn(customer1);
        assertEquals(customer1,customerServiceImpl.saveCustomerDetails(customer));
    }

    @Test
    @DisplayName("testing saveCustomer() with invalid data")
    public void testSaveWithInValidCustomerData() throws SQLException, IOException {
        Customer customer=new Customer();
        customer.setFirstName("");
        customer.setLastName("");
        customer.setPhoneNumber("");
        assertThrows(InvalidDataFoundException.class,()->customerServiceImpl.saveCustomerDetails(customer));
    }

    @Test
    @DisplayName("testing saveCustomer() when customer already exits")
    public void testSaveWhenCustomerAlreadyExits() throws SQLException, IOException {
        Customer customer=new Customer();
        customer.setCustomerId(20);
        customer.setFirstName("aaa");
        customer.setLastName("bbb");
        customer.setPhoneNumber("9063659874");
        when(customerDaoMock.getCustomerId(customer)).thenReturn(20);
        when(customerDaoMock.getCustomerDetails(20)).thenReturn(customer);
        assertEquals(customer,customerServiceImpl.saveCustomerDetails(customer));
    }

    @Test
    @DisplayName("testing search method with valid customer id")
    public void testSearchWithValidCustomerId() throws SQLException, IOException {
        Customer customer=new Customer();
        customer.setCustomerId(23);
        customer.setFirstName("cccc");
        customer.setLastName("222");
        customer.setPhoneNumber("1020458963");
        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(true);
        when(customerDaoMock.getCustomerDetails(anyInt())).thenReturn(customer);
        assertEquals(customer,customerServiceImpl.searchCustomer(23));
    }

    @ParameterizedTest
    @ValueSource(ints = {10,11,12,13,14})
    @DisplayName("testing search method with invalid customer id")
    public void testSearchWithInValidCustomerId(int customerId) throws SQLException, IOException {
            when(customerDaoMock.findCustomerById(customerId)).thenReturn(false);
            assertThrows(ResourceNotFoundException.class,()->customerServiceImpl.searchCustomer(customerId));
    }

    @Test
    @DisplayName("testing getCustomerDetails() with valid customer id")
    public void testGetCustomerDetailsWithValidCustomerId() throws SQLException, IOException {
        Customer customer=new Customer();
        customer.setCustomerId(23);
        customer.setFirstName("cccc");
        customer.setLastName("222");
        customer.setPhoneNumber("1020458963");
        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(true);
        when(customerDaoMock.getCustomerDetails(anyInt())).thenReturn(customer);
        assertEquals(customer,customerServiceImpl. getCustomerDetails(23));
    }

    @Test
    @DisplayName("testing getCustomerDetails() with invalid customer id")
    public void testGetCustomerDetailsWithInValidCustomerId() throws SQLException, IOException {
        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(false);
        assertThrows(ResourceNotFoundException.class,()->customerServiceImpl.getCustomerDetails(23));
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4})
    @DisplayName("testing deleteCustomerDetails() with valid customer id")
    public void testDeleteCustomerDetailsWithValidCustomerId(int customerId) throws SQLException, IOException {
        when(customerDaoMock.findCustomerById(customerId)).thenReturn(true);
        customerServiceImpl.deleteCustomerDetails(customerId);
        verify(customerDaoMock,times(1)).deleteCustomerDetails(customerId);
    }
    @ParameterizedTest
    @ValueSource(ints = {50,60,70})
    @DisplayName("testing deleteCustomerDetails() with invalid customer id")
    public void testDeleteCustomerDetailsWithInValidCustomerId(int customerId) throws SQLException, IOException {
        when(customerDaoMock.findCustomerById(customerId)).thenReturn(false);
        assertThrows(ResourceNotFoundException.class,()->customerServiceImpl.deleteCustomerDetails(customerId));
    }

    @Test
    @DisplayName("testing updateCustomerDetails() with valid date and valid customer id")
    public void testUpdateCustomerWithValidDataAndValidCustomerId() throws SQLException, IOException {
        Customer customer=new Customer();
        customer.setCustomerId(20);
        customer.setFirstName("aaa");
        customer.setLastName("bbb");
        customer.setPhoneNumber("9063659874");

        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(true);
        customerDaoMock.updateCustomerDetails(customer);
        verify(customerDaoMock,times(1)).updateCustomerDetails(customer);
    }

    @Test
    @DisplayName("testing updateCustomerDetails() with valid data and invalid customer id")
    public void testUpdateCustomerWithValidDataAndInValidCustomerId() throws SQLException, IOException {
        Customer customer=new Customer();
        customer.setCustomerId(20);
        customer.setFirstName("aaa");
        customer.setLastName("bbb");
        customer.setPhoneNumber("9063659874");
        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(false);
        assertThrows(ResourceNotFoundException.class,()->customerServiceImpl.updateCustomerDetails(customer));
    }
    @Test
    @DisplayName("testing updateCustomerDetails() with invalid data and valid customer id")
    public void testUpdateCustomerWithInValidDataAndValidCustomerId() throws SQLException, IOException {
        Customer customer=new Customer();
        customer.setCustomerId(20);
        customer.setFirstName("aaa");
        customer.setLastName("bbb");
        customer.setPhoneNumber("90636");
        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(true);
        assertThrows(InvalidDataFoundException.class,()->customerServiceImpl.updateCustomerDetails(customer));
    }

}