package com.brevitaz.springrest.service;

import com.brevitaz.springrest.dao.CustomerDao;
import com.brevitaz.springrest.dao.OrderDao;
import com.brevitaz.springrest.exception_handler.NoContentException;
import com.brevitaz.springrest.exception_handler.ResourceNotFoundException;
import com.brevitaz.springrest.model.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DisplayName("OrderServiceImplTest class")
//@TestMethodOrder(MethodOrderer.DisplayName.class)
//@TestMethodOrder(MethodOrderer.class)
@TestMethodOrder(MethodOrderer.Random.class)
class OrderServiceImplTest {

    @InjectMocks
    private OrderServiceImpl orderServiceImpl;

    @Mock
    private OrderDao orderDaoMock;

    @Mock
    private CustomerDao customerDaoMock;

    public OrderServiceImplTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("testing getMenu()")
    void testGetMenu() throws SQLException, IOException {
        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item(10, "252", 250));
        itemList.add(new Item(11, "253", 250));
        itemList.add(new Item(12, "254", 250));
        when(orderDaoMock.getMenu()).thenReturn(itemList);
        assertEquals(3, orderServiceImpl.getMenu().size());
    }

    @Test
    @DisplayName("testing getAllOrder() with valid customer id")
    public void testGetAllOrderWithValidCustomerId() throws SQLException, IOException {
        Page page = new Page();
        Sort sort = new Sort();

        List<Order> list = new ArrayList<>();
        list.add(new Order());
        list.add(new Order());
        list.add(new Order());

        when(customerDaoMock.findCustomerById(23)).thenReturn(true);
        when(orderDaoMock.getAllOrder(23, page, sort)).thenReturn(list);
        assertEquals(3, orderServiceImpl.getAllOrder(23, page, sort).size());
    }

    @Test
    @DisplayName("testing getAllOrder() with invalid customer id")
    public void testGetAllOrderWithInValidCustomerId() throws SQLException, IOException {
        Page page = new Page();
        Sort sort = new Sort();
        when(customerDaoMock.findCustomerById(23)).thenReturn(false);
        assertThrows(ResourceNotFoundException.class, () -> orderServiceImpl.getAllOrder(23, page, sort));
    }

    @Test
    @DisplayName("testing getAllOrderItem() with valid customer id")
    public void testGetAllOrderItemWithValidOrderId() throws SQLException, IOException {
        Page page = new Page();
        Sort sort = new Sort();
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(new OrderItem());
        orderItems.add(new OrderItem());
        orderItems.add(new OrderItem());
        orderItems.add(new OrderItem());
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(true);
        when(orderDaoMock.getAllOrderItem(50, page, sort)).thenReturn(orderItems);
        assertEquals(4, orderServiceImpl.getAllOrderItem(50, page, sort).size());
    }

    @Test
    @DisplayName("testing getAllOrderItem() with invalid customer id")
    public void testGetAllOrderItemWithInValidOrderId() throws SQLException, IOException {
        Page page = new Page();
        Sort sort = new Sort();
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(false);
        assertThrows(ResourceNotFoundException.class, () -> orderServiceImpl.getAllOrderItem(50, page, sort));
    }

    @Test
    @DisplayName("testing cancelOrder() with valid order id")
    public void testCancelOrderWithValidOrderId() throws SQLException, IOException {
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(true);
        orderServiceImpl.cancelOrder(30);
        verify(orderDaoMock, times(1)).deleteOrder(30);
    }

    @Test
    @DisplayName("testing cancelOrder() with invalid order id")
    public void testCancelOrderWithInValidOrderId() throws SQLException, IOException {
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(false);
        assertThrows(ResourceNotFoundException.class,()->orderServiceImpl.cancelOrder(30));
    }

    @Test
    @DisplayName("testing removeOrderItem() with valid data")
    public void testRemoveOrderItemWithValidData() throws SQLException, IOException {
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(true);
        when(orderDaoMock.findItemById(anyInt())).thenReturn(true);
        orderServiceImpl.removeOrderItem(20,3);
        verify(orderDaoMock,times(1)).deleteOrderItem(20,3);
    }

    @Test
    @DisplayName("testing removeOrderItem() with invalid item id")
    public void testRemoveOrderItemWithInValidItemId() throws SQLException, IOException {
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(true);
        when(orderDaoMock.findItemById(anyInt())).thenReturn(false);
        assertThrows(ResourceNotFoundException.class,()->orderServiceImpl.removeOrderItem(30,20));
    }

    @Test
    @DisplayName("testing removeOrderItem() with invalid order id")
    public void testRemoveOrderItemWithInValidOrderId() throws SQLException, IOException {
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(false);
        when(orderDaoMock.findItemById(anyInt())).thenReturn(true);
        assertThrows(ResourceNotFoundException.class,()->orderServiceImpl.removeOrderItem(30,20));
    }

    @Test
    @DisplayName("testing changeOrderItemDetails() with valid data")
    public void testChangeOrderItemDetailsWithValidData() throws SQLException, IOException {
        Order order=new Order();
        order.setOrderId(1);
        OrderItem orderItem=new OrderItem();
        orderItem.setItemId(1);
        orderItem.setQuantity(5);
        orderItem.setOrder(order);
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(true);
        when(orderDaoMock.findItemById(anyInt())).thenReturn(true);
        orderServiceImpl.changeOrderItem(orderItem);
        verify(orderDaoMock,times(1)).updateOrderItem(orderItem);
    }

    @Test
    @DisplayName("testing changeOrderItemDetails() with invalid order id")
    public void testChangeOrderItemDetailsWithInValidOrderId() throws SQLException, IOException {
        Order order=new Order();
        order.setOrderId(1);
        OrderItem orderItem=new OrderItem();
        orderItem.setItemId(1);
        orderItem.setQuantity(5);
        orderItem.setOrder(order);
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(false);
        when(orderDaoMock.findItemById(anyInt())).thenReturn(true);
        assertThrows(ResourceNotFoundException.class,()->orderServiceImpl.changeOrderItem(orderItem));
    }

    @Test
    @DisplayName("testing changeOrderItemDetails() with invalid item id")
    public void testChangeOrderItemDetailsWithInValidItemId() throws SQLException, IOException {
        Order order=new Order();
        order.setOrderId(1);
        OrderItem orderItem=new OrderItem();
        orderItem.setItemId(1);
        orderItem.setQuantity(5);
        orderItem.setOrder(order);
        when(orderDaoMock.findOrderById(anyInt())).thenReturn(true);
        when(orderDaoMock.findItemById(anyInt())).thenReturn(false);
        assertThrows(ResourceNotFoundException.class,()->orderServiceImpl.changeOrderItem(orderItem));
    }

    @Test
    @DisplayName("testing searchOrder() with valid data")
    public void testSearchOrderWithValidData() throws SQLException, IOException {
        Page page = new Page();
        Sort sort = new Sort();
        List<Order> list = new ArrayList<>();
        list.add(new Order());
        list.add(new Order());
        list.add(new Order());
        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(true);
        when(orderDaoMock.getAllOrder(23, page, sort)).thenReturn(list);
        assertEquals(3, orderServiceImpl.getAllOrder(23, page, sort).size());
    }

    @Test
    @DisplayName("testing searchOrder() with invalid customer id")
    public void testSearchOrderWithInValidCustomerId() throws SQLException, IOException {
        Page page = new Page();
        Sort sort = new Sort();
        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(false);
        assertThrows(ResourceNotFoundException.class, () -> orderServiceImpl.getAllOrder(23, page, sort));
    }
    @Test
    @DisplayName("testing searchOrder() and return empty list")
    public void testSearchOrderAndReturnEmptyList() throws SQLException, IOException {
        Page page = new Page();
        Sort sort = new Sort();
        List<Order> list = new ArrayList<>();
        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(true);
        when(orderDaoMock.getAllOrder(23, page, sort)).thenReturn(list);
        assertThrows(NoContentException.class,()->orderServiceImpl.searchOrder(23, page, sort));
    }

    @Test
    @DisplayName("testing placeOrder() with valid data")
    public void testPlaceOrderWithValidData() throws SQLException, IOException {
        Order order=new Order();

        Customer customer=new Customer();
        customer.setCustomerId(1);

        OrderItem orderItem=new OrderItem();
        orderItem.setItemId(1);
        orderItem.setQuantity(500);

        List<OrderItem> orderItemList=new ArrayList<>();
        orderItemList.add(orderItem);

        Item item=new Item();
        item.setItemId(1);
        item.setItemName("pizza");
        item.setItemPrice(250);

        order.setCustomer(customer);
        order.setOrderItems(orderItemList);

        when(customerDaoMock.findCustomerById(1)).thenReturn(true);
        when(orderDaoMock.findItemById(1)).thenReturn(true);
        when(orderDaoMock.getItemDetails(any(Item.class))).thenReturn(item);

        orderServiceImpl.placeOrder(order);

        verify(orderDaoMock,times(1)).saveOrder(order);
        verify(orderDaoMock,times(1)).saveOrderItem(orderItem);
    }
    @Test
    @DisplayName("testing placeOrder() with invalid customer id")
    public void testPlaceOrderWithInValidCustomerId() throws SQLException, IOException {
        Order order=new Order();

        Customer customer=new Customer();
        customer.setCustomerId(1);

        OrderItem orderItem=new OrderItem();
        orderItem.setItemId(1);
        orderItem.setQuantity(500);

        List<OrderItem> orderItemList=new ArrayList<>();
        orderItemList.add(orderItem);

        Item item=new Item();
        item.setItemId(1);
        item.setItemName("pizza");
        item.setItemPrice(250);

        order.setCustomer(customer);
        order.setOrderItems(orderItemList);

        when(customerDaoMock.findCustomerById(anyInt())).thenReturn(false);
        assertThrows(ResourceNotFoundException.class,()->orderServiceImpl.placeOrder(order));
    }

    @Test
    @DisplayName("testing placeOrder() with invalid item")
    public void testPlaceOrderWithInValidItem() throws SQLException, IOException {
        Order order=new Order();

        Customer customer=new Customer();
        customer.setCustomerId(1);

        OrderItem orderItem=new OrderItem();
        orderItem.setItemId(1);
        orderItem.setQuantity(500);

        List<OrderItem> orderItemList=new ArrayList<>();
        orderItemList.add(orderItem);

        Item item=new Item();
        item.setItemId(1);
        item.setItemName("pizza");
        item.setItemPrice(250);

        order.setCustomer(customer);
        order.setOrderItems(orderItemList);

        when(customerDaoMock.findCustomerById(1)).thenReturn(true);
        when(orderDaoMock.findItemById(1)).thenReturn(false);
        assertThrows(ResourceNotFoundException.class,()->orderServiceImpl.placeOrder(order));
    }
}