package com.brevitaz.springrest.service;

import com.brevitaz.springrest.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface OrderService {
    public List<Item> getMenu() throws SQLException, IOException;

    public void placeOrder(Order order) throws SQLException, IOException;

    public List<Order> getAllOrder(int customerId, Page page, Sort sort) throws SQLException, IOException;

    public List<OrderItem> getAllOrderItem(int orderId, Page page, Sort sort) throws SQLException, IOException;

    public List<Order> searchOrder(int customerId, Page page, Sort sort) throws SQLException, IOException;

    public  void cancelOrder(int orderId) throws SQLException, IOException;

    public void removeOrderItem(int orderId, int itemId) throws SQLException, IOException;

    public void changeOrderItem(OrderItem orderItem) throws SQLException, IOException;

    public List<OrderItem> searchOrderItem(int orderId, Page page, Sort sort) throws SQLException, IOException;

}
