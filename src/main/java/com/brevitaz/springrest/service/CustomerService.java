package com.brevitaz.springrest.service;

import com.brevitaz.springrest.model.Customer;


import java.io.IOException;
import java.sql.SQLException;

public interface CustomerService {

    public Customer saveCustomerDetails(Customer customer) throws SQLException, IOException;

    public Customer searchCustomer(int customerId) throws SQLException, IOException;

    public Customer getCustomerDetails(int customerId) throws SQLException, IOException;

    public void deleteCustomerDetails(int customerId) throws SQLException, IOException;

    public void updateCustomerDetails(Customer customer) throws SQLException, IOException;

}
