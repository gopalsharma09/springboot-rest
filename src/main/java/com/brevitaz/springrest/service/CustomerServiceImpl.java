package com.brevitaz.springrest.service;

import com.brevitaz.springrest.exception_handler.InvalidDataFoundException;
import com.brevitaz.springrest.exception_handler.ResourceNotFoundException;
import com.brevitaz.springrest.dao.CustomerDao;
import com.brevitaz.springrest.model.Customer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;


@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    private static final Logger logger=Logger.getLogger(CustomerServiceImpl.class.getName());

    @Override
    public Customer saveCustomerDetails(Customer customer) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.CustomerServiceImpl::saveCustomerDetails() start");
        if(customer.getFirstName().isEmpty() || customer.getLastName().isEmpty() ||
                customer.getPhoneNumber().isEmpty() || customer.getPhoneNumber().length()!=10) {
            throw new InvalidDataFoundException("Enter Valid Data");
        }
        int customerId=this.customerDao.getCustomerId(customer);
        if(customerId==-1)
            customer=this.customerDao.saveCustomer(customer);
        else
            customer=this.customerDao.getCustomerDetails(customerId);
        return customer;
    }

    @Override
    public Customer searchCustomer(int customerId) throws SQLException , IOException{
        logger.debug("com.brevitaz.springrest.service.CustomerServiceImpl::searchCustomer() start");

        if(!this.customerDao.findCustomerById(customerId)){
            logger.info("Customer Not found with id="+customerId);
            throw new ResourceNotFoundException("Customer Not Found With id="+customerId);
        }
        return this.customerDao.getCustomerDetails(customerId);
    }

    @Override
    public Customer getCustomerDetails(int customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.CustomerServiceImpl::getCustomerDetails() start");
        if(!this.customerDao.findCustomerById(customerId))
            throw new ResourceNotFoundException("Customer Not Found With id="+customerId);
        this.customerDao.getCustomerDetails(customerId);
        return this.customerDao.getCustomerDetails(customerId);
    }

    @Override
    public void deleteCustomerDetails(int customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.CustomerServiceImpl::deleteCustomerDetails() start");
        if(!this.customerDao.findCustomerById(customerId))
            throw new ResourceNotFoundException("Customer Not Found With id="+customerId);
        this.customerDao.deleteCustomerDetails(customerId);
    }

    @Override
    public void updateCustomerDetails(Customer customer) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.CustomerServiceImpl::updateCustomerDetails() start");
        if(!this.customerDao.findCustomerById(customer.getCustomerId()))
            throw new ResourceNotFoundException("Customer Not Found With id="+customer.getCustomerId());
        if(customer.getFirstName().isEmpty() || customer.getLastName().isEmpty() ||
                customer.getPhoneNumber().isEmpty() || customer.getPhoneNumber().length()!=10)
            throw new InvalidDataFoundException("Enter Valid Data");
        this.customerDao.updateCustomerDetails(customer);
    }
}
