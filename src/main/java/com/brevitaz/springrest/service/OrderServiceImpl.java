package com.brevitaz.springrest.service;

import com.brevitaz.springrest.exception_handler.NoContentException;
import com.brevitaz.springrest.exception_handler.ResourceNotFoundException;
import com.brevitaz.springrest.dao.CustomerDao;
import com.brevitaz.springrest.dao.OrderDao;
import com.brevitaz.springrest.model.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService{
    @Autowired
    private OrderDao orderDao;

    @Autowired
    private CustomerDao customerDao;

    private static Logger logger=Logger.getLogger(OrderServiceImpl.class.getName());

    @Override
    public List<Item> getMenu() throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::getMenu() start");
        List<Item> itemList=orderDao.getMenu();
        return itemList;
    }

    @Override
    public void placeOrder(Order order) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::placeOrder() start");
        if(!this.customerDao.findCustomerById(order.getCustomer().getCustomerId()))
            throw new ResourceNotFoundException("Customer Not Found With id="+order.getCustomer().getCustomerId());

        List<OrderItem> orderItems=order.getOrderItems();

        for(OrderItem orderItem:orderItems) {
            if (!this.orderDao.findItemById(orderItem.getItemId()))
                throw new ResourceNotFoundException("itemId :"+orderItem.getItemId()+" not found in menu");
        }
        orderDao.saveOrder(order);
        placeOrderItem(order,orderItems);

    }

    public void placeOrderItem(Order order, List<OrderItem> orderItems) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::placeOrderItem() start");
        order=this.orderDao.getOrder(order);
        for(OrderItem orderItem:orderItems) {
            Item item = new Item();
            item.setItemId(orderItem.getItemId());
            item = orderDao.getItemDetails(item);
            orderItem.setItemName(item.getItemName());
            orderItem.setItemPrice(item.getItemPrice());
            orderItem.setQuantity(orderItem.getQuantity());
            orderItem.setOrder(order);
            orderDao.saveOrderItem(orderItem);
        }
    }

    @Override
    public List<Order> getAllOrder(int customerId, Page page, Sort sort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::getAllOrder() start");
        if(!this.customerDao.findCustomerById(customerId))
            throw new ResourceNotFoundException("Customer Not Found With id="+customerId);
        List<Order> orders=orderDao.getAllOrder(customerId,page,sort);
        return orders;
    }

    @Override
    public List<OrderItem> getAllOrderItem(int orderId, Page page, Sort sort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::getAllOrderItem() start");
        if(!this.orderDao.findOrderById(orderId))
            throw new ResourceNotFoundException("Order Not Found With id="+orderId);
        List<OrderItem> orderItems=orderDao.getAllOrderItem(orderId,page,sort);
        return orderItems;
    }

    @Override
    public void cancelOrder(int orderId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::cancelOrder() start");
        if(!this.orderDao.findOrderById(orderId))
            throw new ResourceNotFoundException("Order Not Found With id="+orderId);
        this.orderDao.deleteOrder(orderId);
    }

    @Override
    public void removeOrderItem(int orderId, int itemId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::removeOrderItem() start");
        if(!this.orderDao.findOrderById(orderId))
            throw new ResourceNotFoundException("Order Not Found With id="+orderId);
        if(!this.orderDao.findItemById(itemId))
            throw new ResourceNotFoundException("Item not found with itemId="+itemId);
        this.orderDao.deleteOrderItem(orderId,itemId);
    }

    @Override
    public void changeOrderItem(OrderItem orderItem) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::changeOrderItem() start");
        if(!this.orderDao.findOrderById(orderItem.getOrder().getOrderId()))
            throw new ResourceNotFoundException("Order Not Found With Order id="+orderItem.getOrder().getOrderId());
        if(!this.orderDao.findItemById(orderItem.getItemId()))
            throw new ResourceNotFoundException("Item not found with itemId="+orderItem.getItemId());
        this.orderDao.updateOrderItem(orderItem);
    }

    @Override
    public List<Order> searchOrder(int customerId,Page page, Sort sort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::searchOrder() start");
        if(!this.customerDao.findCustomerById(customerId))
            throw new ResourceNotFoundException("Customer Not Found With id="+customerId);
        List<Order> orders=orderDao.getAllOrder(customerId, page, sort);
        if(orders.size()==0)
            throw new NoContentException("Customer Not Ordered items");
        return orders;
    }

    @Override
    public List<OrderItem> searchOrderItem(int orderId, Page page, Sort sort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.service.OrderServiceImpl::searchOrderItem() start");
        if(!this.orderDao.findOrderById(orderId))
            throw new ResourceNotFoundException("Order Not Found With id="+orderId);
        List<OrderItem> orderItems=orderDao.getAllOrderItem(orderId, page, sort);
        if(orderItems.size()==0)
            throw new NoContentException("Customer Not Ordered items");
        return orderItems;
    }
}
