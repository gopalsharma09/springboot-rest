package com.brevitaz.springrest;

import com.brevitaz.springrest.dao.CustomerDaoImpl;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringrestApplication {

	private static final Logger logger=Logger.getLogger(SpringrestApplication.class.getName());

	static {
		try {
			PropertyConfigurator.configure("src/main/resources/log4j.properties");
			logger.info("com.brevitaz.springrest.SpringrestApplication::log4j Setup ready");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.fatal("com.brevitaz.springrest.SpringrestApplication:: Problem while setting up log4j "+e.getMessage());
		}
	}
	public static void main(String[] args) {
		logger.debug("com.brevitaz.springrest.SpringrestApplication::main(-) start");
		SpringApplication.run(SpringrestApplication.class, args);
	}

}
