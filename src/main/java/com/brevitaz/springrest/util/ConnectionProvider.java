package com.brevitaz.springrest.util;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.stereotype.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class ConnectionProvider {

	private static Logger logger=Logger.getLogger(ConnectionProvider.class.getName());
//
//	static {
//		try {
//			PropertyConfigurator.configure("src/main/resources/log4j.properties");
//			logger.info("com.brevitaz.springrest.service.CustomerServiceImpl::log4j Setup ready");
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			logger.fatal("com.brevitaz.springrest.service.CustomerServiceImpl:: Unsuccessful log4j SetUp "+e.getMessage());
//		}
//	}

	public Connection getConnection()
	{
		logger.debug("com.brevitaz.springrest.util.ConnectionProvider::getConnection() start");
		Connection connection=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			logger.info("com.brevitaz.springrest.util.ConnectionProvider::JDBC Driver class loaded");
			connection=DriverManager.getConnection("jdbc:mysql://localhost/brevitazjdbcproject","root","root");
			logger.info("com.brevitaz.springrest.util.ConnectionProvider::Connection is established with D/B");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			logger.fatal("com.brevitaz.springrest.util.ConnectionProvider:: known D/B problem"+e.getMessage());
		}
		return connection;
	}
}
