package com.brevitaz.springrest.model;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

public class Order {
    private int orderId;
    private Customer customer;
    private Date date;
    private List<OrderItem> orderItems;

    public Order() { }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getOrderId() == order.getOrderId() && Objects.equals(getCustomer(), order.getCustomer()) && Objects.equals(getDate(), order.getDate()) && Objects.equals(getOrderItems(), order.getOrderItems());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrderId(), getCustomer(), getDate(), getOrderItems());
    }

}
