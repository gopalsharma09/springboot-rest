package com.brevitaz.springrest.model;

import java.util.Date;

public class ErrorDetails {
    private Date timesTemp;
    private String message;
    private String details;

    public ErrorDetails(Date timesTemp, String message, String details) {
        this.timesTemp = timesTemp;
        this.message = message;
        this.details = details;
    }

    public Date getTimesTemp() {
        return timesTemp;
    }

    public void setTimesTemp(Date timesTemp) {
        this.timesTemp = timesTemp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
