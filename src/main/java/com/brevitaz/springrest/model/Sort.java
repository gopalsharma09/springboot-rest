package com.brevitaz.springrest.model;

public class Sort {

    private SortFiled sortFiled;
    private SortingOrder sortOrder;

    public enum SortFiled{ORDER_ID,ORDER_DATE}

    public enum SortingOrder{ASC,DESC}

    public Sort() {
        this.sortFiled=SortFiled.ORDER_ID;
        this.sortOrder=SortingOrder.ASC;
    }

    public SortFiled getSortFiled() {
        return sortFiled;
    }

    public void setSortFiled(SortFiled sortFiled) {
        this.sortFiled = sortFiled;
    }

    public SortingOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortingOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
