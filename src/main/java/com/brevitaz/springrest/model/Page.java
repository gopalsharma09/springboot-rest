package com.brevitaz.springrest.model;

import java.util.Objects;

public class Page {
    private int pageNo;
    private int size;

    public Page() {
        this.size =10;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Page)) return false;
        Page page = (Page) o;
        return getPageNo() == page.getPageNo() && getSize() == page.getSize();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPageNo(), getSize());
    }
}
