package com.brevitaz.springrest.model;

import java.util.Objects;

public class OrderItem {

    private int itemId;
    private String itemName;
    private int itemPrice;
    private int quantity;
    private Order order;

    public OrderItem() { }

    public OrderItem(int itemId, String itemName, int itemPrice, int quantity, Order order) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.quantity = quantity;
        this.order = order;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderItem)) return false;
        OrderItem orderItem = (OrderItem) o;
        return getItemId() == orderItem.getItemId() && getItemPrice() == orderItem.getItemPrice() && getQuantity() == orderItem.getQuantity() && Objects.equals(getItemName(), orderItem.getItemName()) && Objects.equals(getOrder(), orderItem.getOrder());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getItemId(), getItemName(), getItemPrice(), getQuantity(), getOrder());
    }
}
