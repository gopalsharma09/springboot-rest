package com.brevitaz.springrest.exception_handler;

public class InvalidDataFoundException extends RuntimeException{
    private static final long serialVersionUID=1L;

    public InvalidDataFoundException(String message) {

        super(message);
    }
}
