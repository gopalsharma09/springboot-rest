package com.brevitaz.springrest.exception_handler;

import com.brevitaz.springrest.model.ErrorDetails;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static Logger logger=Logger.getLogger(GlobalExceptionHandler.class.getName());

    //handle specific exception
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException resourceNotFoundException, WebRequest webRequest)
    {
        ErrorDetails errorDetails=new ErrorDetails(new Date(), resourceNotFoundException.getMessage(),webRequest.getDescription(false));
        return new ResponseEntity(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidDataFoundException.class)
    public ResponseEntity<?> handleInvalidDataFoundException(InvalidDataFoundException invalidDataFoundException, WebRequest webRequest)
    {
        ErrorDetails errorDetails=new ErrorDetails(new Date(), invalidDataFoundException.getMessage(),webRequest.getDescription(false));
        return new ResponseEntity(errorDetails, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(NoContentException.class)
    public ResponseEntity<?> handleNoContentException(NoContentException noContentException, WebRequest webRequest)
    {
        ErrorDetails errorDetails=new ErrorDetails(new Date(), noContentException.getMessage(),webRequest.getDescription(false));
        return new ResponseEntity(errorDetails, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(ResourceAlreadyExistException.class)
    public ResponseEntity<?> handleResourceAlreadyExistException(ResourceAlreadyExistException resourceAlreadyExistException, WebRequest webRequest)
    {
        ErrorDetails errorDetails=new ErrorDetails(new Date(), resourceAlreadyExistException.getMessage(),webRequest.getDescription(false));
        return new ResponseEntity(errorDetails, HttpStatus.CONFLICT);
    }

    //handle global Exception
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception exception, WebRequest webRequest) throws IOException {
        ErrorDetails errorDetails=new ErrorDetails(new Date(), exception.getMessage(),webRequest.getDescription(false));
        return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
