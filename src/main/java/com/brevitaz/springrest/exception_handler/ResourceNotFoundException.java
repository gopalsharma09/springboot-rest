package com.brevitaz.springrest.exception_handler;


public class ResourceNotFoundException extends RuntimeException{

    private static final long serialVersionUID=1L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
