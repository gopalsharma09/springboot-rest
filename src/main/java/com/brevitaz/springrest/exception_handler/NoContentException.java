package com.brevitaz.springrest.exception_handler;

public class NoContentException extends RuntimeException{
    private static final long serialVersionUID=1L;

    public NoContentException(String message) {
        super(message);
    }
}
