package com.brevitaz.springrest.dao;

import com.brevitaz.springrest.model.Customer;
import com.brevitaz.springrest.util.ConnectionProvider;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class CustomerDaoImpl implements CustomerDao{

    @Autowired
    private ConnectionProvider connectionProvider;

    private static final Logger logger=Logger.getLogger(CustomerDaoImpl.class.getName());

    private static final String INSERT_CUSTOMER ="insert into customer(first_name,last_name,phone_number) values (?,?,?)";
    private static final String GET_CUSTOMER_DETAILS="select * from customer where customer_id=?";
    private static final String DELETE_CUSTOMER="delete from customer where customer_id=?";
    private static final String DELETE_ORDER="delete from order_details where customer_id=?";
    private static final String DELETE_ORDER_ITEM="delete from order_item where order_id IN (select order_id from order_details where customer_id=?)";
    private static final String UPDATE_CUSTOMER="update customer set first_name=?,last_name=?,phone_number=? where customer_id=?";
    private static final String GET_CUSTOMER_ID="select customer_id from customer where first_name=? AND last_name=? AND phone_number=?";


    public Customer saveCustomer(Customer customer) throws SQLException, IOException {

        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::saveCustomer() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(INSERT_CUSTOMER);
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::saveCustomer() PreparedStatement object created");
        preparedStatement.setString(1,customer.getFirstName().toLowerCase().trim());
        preparedStatement.setString(2,customer.getLastName().toLowerCase().trim());
        preparedStatement.setString(3,customer.getPhoneNumber().toLowerCase());
        preparedStatement.executeUpdate();
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::saveCustomer() SQL query is send to DB s/w for execution");
        return getCustomer(customer);
    }

    private Customer getCustomer(Customer customer) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomer() start");
        int customerId=getCustomerId(customer);
        customer.setCustomerId(customerId);
        return customer;
    }

    public int getCustomerId(Customer customer) throws SQLException, IOException {

        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomerId() start");
        int customerId=-1;
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_CUSTOMER_ID);
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomerId() PreparedStatement object created");
        preparedStatement.setString(1,customer.getFirstName().toLowerCase().trim());
        preparedStatement.setString(2,customer.getLastName().toLowerCase().trim());
        preparedStatement.setString(3,customer.getPhoneNumber().toLowerCase());
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomerId() SQL query is send to DB s/w for execution and ResultSet Object is created");
        while(resultSet.next())
            customerId=resultSet.getInt("customer_id");
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomerId() ResultSet object is processed");
        return customerId;
    }

    public Customer getCustomerDetails(int customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomerDetails() start");
        Customer customer=new Customer();
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_CUSTOMER_DETAILS);
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomerDetails() PreparedStatement object created");
        preparedStatement.setInt(1,customerId);
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomerDetails() SQL query is send to DB s/w for execution and ResultSet Object is created");
        while(resultSet.next())
        {
            customer.setCustomerId(resultSet.getInt("customer_id"));
            customer.setFirstName(resultSet.getString("first_name"));
            customer.setLastName(resultSet.getString("last_name"));
            customer.setPhoneNumber(resultSet.getString("phone_number"));
        }
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::getCustomerDetails() ResultSet object is processed");
        return customer;
    }

    public void deleteCustomerDetails(int customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::deleteCustomerDetails() start");
        PreparedStatement firstPreparedStatement=connectionProvider.getConnection().prepareStatement(DELETE_ORDER_ITEM);
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::deleteCustomerDetails() first PreparedStatement object created");
        PreparedStatement secondPreparedStatement =connectionProvider.getConnection().prepareStatement(DELETE_ORDER);
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::deleteCustomerDetails() second PreparedStatement object created");
        PreparedStatement thirdPreparedStatement =connectionProvider.getConnection().prepareStatement(DELETE_CUSTOMER);
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::deleteCustomerDetails() third PreparedStatement object created");
        firstPreparedStatement.setInt(1,customerId);
        firstPreparedStatement.executeUpdate();
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::deleteCustomerDetails() first SQL query is send to DB s/w for execution");
        secondPreparedStatement.setInt(1,customerId);
        secondPreparedStatement.executeUpdate();
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::deleteCustomerDetails() second SQL query is send to DB s/w for execution");
        thirdPreparedStatement.setInt(1,customerId);
        thirdPreparedStatement.executeUpdate();
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::deleteCustomerDetails() third SQL query is send to DB s/w for execution");
    }

    public void updateCustomerDetails(Customer customer) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::updateCustomerDetails() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(UPDATE_CUSTOMER);
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::updateCustomerDetails() PreparedStatement object created");
        preparedStatement.setString(1,customer.getFirstName().toLowerCase());
        preparedStatement.setString(2,customer.getLastName().toLowerCase());
        preparedStatement.setString(3,customer.getPhoneNumber().toLowerCase());
        preparedStatement.setInt(4,customer.getCustomerId());
        preparedStatement.executeUpdate();
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::updateCustomerDetails()  SQL query is send to DB s/w for execution");
    }
    @Override
    public boolean findCustomerById(int customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::findCustomerById() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_CUSTOMER_DETAILS);
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::findCustomerById() PreparedStatement object created");
        preparedStatement.setInt(1,customerId);
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.CustomerDaoImpl::findCustomerById() SQL query is send to DB s/w for execution and ResultSet Object is created");
        return resultSet.next();
    }
}
