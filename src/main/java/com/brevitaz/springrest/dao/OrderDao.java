package com.brevitaz.springrest.dao;

import com.brevitaz.springrest.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface OrderDao {
    List<Item> getMenu() throws SQLException, IOException;

    void saveOrder(Order order) throws SQLException, IOException;

    Order getOrder(Order order) throws SQLException, IOException;

    Item getItemDetails(Item item) throws SQLException, IOException;

    void saveOrderItem(OrderItem orderItem) throws SQLException, IOException;

    List<Order> getAllOrder(int customerId, Page page, Sort sort) throws SQLException, IOException;

    List<OrderItem> getAllOrderItem(int orderId, Page page, Sort sort) throws SQLException, IOException;

    void deleteOrder(int orderId) throws SQLException, IOException;

    void deleteOrderItem(int orderId, int itemId) throws SQLException, IOException;

    void updateOrderItem(OrderItem orderItem) throws SQLException, IOException;

    boolean findItemById(int itemId) throws SQLException, IOException;

    boolean findOrderById(int orderId) throws SQLException, IOException;


}
