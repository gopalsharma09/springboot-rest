package com.brevitaz.springrest.dao;

import com.brevitaz.springrest.model.Customer;

import java.io.IOException;
import java.sql.SQLException;

public interface CustomerDao {
    Customer saveCustomer(Customer customer) throws SQLException, IOException;

    Customer getCustomerDetails(int customerId) throws SQLException, IOException;

    void deleteCustomerDetails(int customerId) throws SQLException, IOException;

    void updateCustomerDetails(Customer customer) throws SQLException, IOException;

    int getCustomerId(Customer customer) throws SQLException, IOException;

    boolean findCustomerById(int customerId) throws SQLException, IOException;

}
