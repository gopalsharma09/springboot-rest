package com.brevitaz.springrest.dao;

import com.brevitaz.springrest.model.*;
import com.brevitaz.springrest.util.ConnectionProvider;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


@Repository
public class OrderDaoImpl implements OrderDao{

    @Autowired
    private ConnectionProvider connectionProvider;

    private static Logger logger= Logger.getLogger(OrderDaoImpl.class.getName());

    private static final String GET_MENU="select * from menu";
    private static final String INSERT_ORDER="insert into order_details (order_date,customer_id) values(?,?)";
    private static final String INSERT_ORDER_ITEM="insert into order_item(item_name,item_price,quantity,order_id) values(?,?,?,?)";
    private static final String GET_ORDER="select order_id from order_details where customer_id=? AND order_date=?";
    private static final String GET_ITEM_DETAILS="select * from menu where id=?";
    private static final String GET_ALL_ORDER="select * from order_details where customer_id=? ORDER BY ? ? LIMIT ? OFFSET ?";
    private static final String GET_ALL_ORDER_ITEM="select * from order_item where order_id=? ORDER BY ? ? LIMIT ? OFFSET ? ";
    private static final String DELETE_ORDER="delete from order_details where order_id=?";
    private static final String DELETE_ORDER_ITEMS="delete from order_item where order_id=?";
    private static final String DELETE_ORDER_ITEM="delete from order_item where order_id=? AND id=?";
    private static final String UPDATE_ORDER_ITEM="update order_item set quantity=? where order_id=? AND id=?";
    private static final String FIND_ORDER_BY_ID="select * from order_details where order_id=?";

    public List<Item> getMenu() throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getMenu() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_MENU);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getMenu() PreparedStatement object created");
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getMenu() SQL query is send to DB s/w for execution and ResultSet Object is created");
        List<Item> listOfItem=new ArrayList<>();
        while(resultSet.next())
        {
            Item item=new Item();
            item.setItemId(resultSet.getInt("id"));
            item.setItemName(resultSet.getString("item_name"));
            item.setItemPrice(resultSet.getInt("item_price"));
            listOfItem.add(item);
        }
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getMenu() ResultSet object is processed");
        return listOfItem;
    }

    public void saveOrder(Order order) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::saveOrder() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(INSERT_ORDER);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::saveOrder() PreparedStatement object created");
        preparedStatement.setDate(1,order.getDate());
        preparedStatement.setInt(2,order.getCustomer().getCustomerId());
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::saveOrder() SQL query is send to DB s/w for execution");
        preparedStatement.execute();
    }

    public void saveOrderItem(OrderItem orderItem) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::saveOrderItem() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(INSERT_ORDER_ITEM);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::saveOrderItem() PreparedStatement object created");
        preparedStatement.setString(1,orderItem.getItemName().toLowerCase());
        preparedStatement.setInt(2,orderItem.getItemPrice());
        preparedStatement.setInt(3,orderItem.getQuantity());
        preparedStatement.setInt(4,orderItem.getOrder().getOrderId());
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::saveOrderItem() SQL query is send to DB s/w for execution");
        preparedStatement.execute();
    }

    public Order getOrder(Order order) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getOrder() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_ORDER);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getOrder() PreparedStatement object created");
        preparedStatement.setInt(1,order.getCustomer().getCustomerId());
        preparedStatement.setDate(2,order.getDate());
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getOrder() SQL query is send to DB s/w for execution and ResultSet Object is created");
        while(resultSet.next()){
            order.setOrderId(resultSet.getInt("order_id"));
        }
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getOrder() ResultSet object is processed");
        return order;
    }

    public Item getItemDetails(Item item) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getItemDetails() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_ITEM_DETAILS);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getItemDetails() PreparedStatement object created");
        preparedStatement.setInt(1,item.getItemId());
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getItemDetails() SQL query is send to DB s/w for execution and ResultSet Object is created");
        while(resultSet.next())
        {
            item.setItemId(resultSet.getInt("id"));
            item.setItemName(resultSet.getString("item_name"));
            item.setItemPrice(resultSet.getInt("item_price"));
        }
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getItemDetails() ResultSet object is processed");
        return item;
    }

    public List<Order> getAllOrder(int customerId, Page page, Sort sort) throws SQLException, IOException {

        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getAllOrder() start");
        List<Order> orders=new ArrayList<>();
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_ALL_ORDER);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getAllOrder() PreparedStatement object created");
        int offSet=(page.getPageNo()-1)*page.getSize();
        preparedStatement.setInt(1,customerId);
        preparedStatement.setString(2,sort.getSortFiled().toString().toLowerCase(Locale.ROOT));
        preparedStatement.setString(3, sort.getSortOrder().toString());
        preparedStatement.setInt(4,page.getSize());
        preparedStatement.setInt(5,offSet);
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getAllOrder() SQL query is send to DB s/w for execution and ResultSet Object is created");
        while(resultSet.next()){
            Order order=new Order();
            Customer customer=new Customer();
            customer.setCustomerId(resultSet.getInt("customer_id"));
            order.setOrderId(resultSet.getInt("order_id"));
            order.setCustomer(customer);
            order.setDate(resultSet.getDate("order_date"));
            orders.add(order);
        }
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getAllOrder() ResultSet object is processed");
        return orders;

    }

    public List<OrderItem> getAllOrderItem(int orderId, Page page, Sort sort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getAllOrderItem() start");
        List<OrderItem> orderItems=new ArrayList<>();
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_ALL_ORDER_ITEM);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getAllOrderItem() PreparedStatement object created");
        int offSet=(page.getPageNo()-1)*page.getSize();
        preparedStatement.setInt(1,orderId);
        preparedStatement.setString(2,sort.getSortFiled().toString().toLowerCase());
        preparedStatement.setString(3, sort.getSortOrder().toString());
        preparedStatement.setInt(4,page.getSize());
        preparedStatement.setInt(5,offSet);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getAllOrderItem() SQL query is send to DB s/w for execution and ResultSet Object is created");
        ResultSet resultSet=preparedStatement.executeQuery();
        while(resultSet.next()){
            OrderItem orderItem=new OrderItem();
            Order order=new Order();
            order.setOrderId(orderId);
            orderItem.setItemId(resultSet.getInt("id"));
            orderItem.setItemName(resultSet.getString("item_name"));
            orderItem.setItemPrice(resultSet.getInt("item_price"));
            orderItem.setQuantity(resultSet.getInt("quantity"));
            orderItem.setOrder(order);
            orderItems.add(orderItem);
        }
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::getAllOrderItem() ResultSet object is processed");
        return orderItems;

    }

    public void deleteOrderItem(int orderId, int itemId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::deleteOrderItem() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(DELETE_ORDER_ITEM);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::deleteOrderItem() PreparedStatement object created");
        preparedStatement.setInt(1,orderId);
        preparedStatement.setInt(2,itemId);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::deleteOrderItem() SQL query is send to DB s/w for execution ");
        preparedStatement.executeUpdate();
    }
    public void deleteOrder(int orderId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::deleteOrder() start");
        PreparedStatement firstPreparedStatement=connectionProvider.getConnection().prepareStatement(DELETE_ORDER_ITEMS);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::deleteOrderItem() first PreparedStatement object created");
        PreparedStatement secondPreparedStatement=connectionProvider.getConnection().prepareStatement(DELETE_ORDER);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::deleteOrderItem() second PreparedStatement object created");
        firstPreparedStatement.setInt(1,orderId);
        firstPreparedStatement.executeUpdate();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::deleteOrder() first SQL query is send to DB s/w for execution");
        secondPreparedStatement.setInt(1,orderId);
        secondPreparedStatement.executeUpdate();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::deleteOrder() second SQL query is send to DB s/w for execution ");
    }

    public void updateOrderItem(OrderItem orderItem) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::updateOrderItem() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(UPDATE_ORDER_ITEM);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::updateOrderItem() PreparedStatement object created");
        preparedStatement.setInt(1,orderItem.getQuantity());
        preparedStatement.setInt(2,orderItem.getOrder().getOrderId());
        preparedStatement.setInt(3,orderItem.getItemId());
        preparedStatement.executeUpdate();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::updateOrderItem() SQL query is send to DB s/w for execution ");
    }

    public boolean findItemById(int itemId) throws SQLException, IOException {

        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::findItemById() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(GET_ITEM_DETAILS);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::findItemById() PreparedStatement object created");
        preparedStatement.setInt(1,itemId);
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::findItemById() SQL query is send to DB s/w for execution and ResultSet Object is created");
        return resultSet.next();
    }

    public boolean findOrderById(int orderId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::findOrderById() start");
        PreparedStatement preparedStatement=connectionProvider.getConnection().prepareStatement(FIND_ORDER_BY_ID);
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::findOrderById() PreparedStatement object created");
        preparedStatement.setInt(1,orderId);
        ResultSet resultSet=preparedStatement.executeQuery();
        logger.debug("com.brevitaz.springrest.dao.OrderDaoImpl::findOrderById() SQL query is send to DB s/w for execution and ResultSet Object is created");
        return resultSet.next();
    }
}
