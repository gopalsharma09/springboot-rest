package com.brevitaz.springrest.controller;

import com.brevitaz.springrest.model.*;
import com.brevitaz.springrest.service.OrderService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    private static Logger logger=Logger.getLogger(OrderController.class.getName());

    @GetMapping("/menu")
    public List<Item> getMenu() throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::getMenu() start");
        return this.orderService.getMenu();
    }

    @PostMapping("/order")
    public void placeOrder(@RequestBody Order order) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::placeOrder() start");
        long millis=System.currentTimeMillis();
        Date date=new Date(millis);
        order.setDate(date);
        this.orderService.placeOrder(order);
    }

    @PostMapping("/view/order")
    public List<Order> viewOrder(@RequestBody OrderPageSort orderPageSort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::viewOrder() start");
        Page page =new Page();
        Sort sort =new Sort();
        if(orderPageSort.getPage()!=null)
            page = orderPageSort.getPage();
        if(orderPageSort.getSort()!=null) {
            sort = orderPageSort.getSort();
        }
        Customer customer=orderPageSort.getCustomer();
        List<Order> orders=this.orderService.getAllOrder(customer.getCustomerId(),page,sort);
        return orders;
    }
    @PostMapping("/view/order/item")
    public List<OrderItem> viewOrderItem(@RequestBody OrderItemPageSort orderItemPageSort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::viewOrderItem() start");
        Page page =new Page();
        Sort sort =new Sort();
        if(orderItemPageSort.getPage()!=null)
            page = orderItemPageSort.getPage();
        if(orderItemPageSort.getSort()!=null)
            sort=orderItemPageSort.getSort();
        Order order=orderItemPageSort.getOrder();
        List<OrderItem> orderItems=this.orderService.getAllOrderItem(order.getOrderId(),page,sort);
        return orderItems;
    }

    @DeleteMapping("/cancel/order/{orderId}")
    public void cancelOrder(@PathVariable String orderId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::cancelOrder() start");
        this.orderService.cancelOrder(Integer.parseInt(orderId));
    }

    @DeleteMapping("/remove/order/item/{orderId}/{itemId}")
    public void removeOrderItem(@PathVariable String orderId,@PathVariable String itemId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::removeOrderItem() start");
        this.orderService.removeOrderItem(Integer.parseInt(orderId),Integer.parseInt(itemId));
    }

    @PutMapping("/change/order/item")
    public void changeOrderItem(@RequestBody OrderItem orderItem) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::changeOrderItem() start");
        this.orderService.changeOrderItem(orderItem);
    }


    @PostMapping("/search/order")
    public List<Order> searchOrder(@RequestBody OrderPageSort orderPageSort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::searchOrder() start");
        Page page =new Page();
        Sort sort =new Sort();
        if(orderPageSort.getPage()!=null)
            page = orderPageSort.getPage();
        if(orderPageSort.getSort()!=null)
            sort=orderPageSort.getSort();
        Customer customer=orderPageSort.getCustomer();
        List<Order> orders=this.orderService.searchOrder(customer.getCustomerId(),page,sort);
        return orders;
    }

    @PostMapping("/search/order/item")
    public List<OrderItem> searchOrderItem(@RequestBody OrderItemPageSort orderItemPageSort) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.OrderController::searchOrderItem() start");
        Page page =new Page();
        Sort sort =new Sort();
        if(orderItemPageSort.getPage()!=null)
            page = orderItemPageSort.getPage();
        if(orderItemPageSort.getSort()!=null)
            sort=orderItemPageSort.getSort();
        Order order=orderItemPageSort.getOrder();
        List<OrderItem> orderItems=this.orderService.searchOrderItem(order.getOrderId(),page,sort);
        return orderItems;
    }
}
