package com.brevitaz.springrest.controller;

import com.brevitaz.springrest.model.Customer;
import com.brevitaz.springrest.service.CustomerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    private static Logger logger=Logger.getLogger(CustomerController.class.getName());

    @PostMapping("/register")
    public Customer saveCustomerDetails(@RequestBody Customer customer) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.CustomerController::saveCustomerDetails() start");
        Customer customerDetails=this.customerService.saveCustomerDetails(customer);
        return customerDetails;
    }

    @GetMapping("/search/customer/{customerId}")
    public Customer searchCustomer(@PathVariable String customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.CustomerController::searchCustomer() start");
        Customer customer=this.customerService.searchCustomer(Integer.parseInt(customerId));
        return customer;
    }

    @GetMapping("/customer/profile/{customerId}")
    public Customer customerDetails(@PathVariable String customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.CustomerController::customerDetails() start");
        Customer customer =this.customerService.getCustomerDetails(Integer.parseInt(customerId));
        return customer;
    }

    @DeleteMapping("/delete/customer/account/{customerId}")
    public void deleteCustomer(@PathVariable String customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.CustomerController::deleteCustomer() start");
        this.customerService.deleteCustomerDetails(Integer.parseInt(customerId));
    }

    @PutMapping("/update/customer/details/{customerId}")
    public void updateCustomerDetails(@RequestBody Customer customer,@PathVariable String customerId) throws SQLException, IOException {
        logger.debug("com.brevitaz.springrest.controller.CustomerController::updateCustomerDetails() start");
        customer.setCustomerId(Integer.parseInt(customerId));
        this.customerService.updateCustomerDetails(customer);
    }
}
